# Ruby Rakefile
#

require 'xp5k'
require 'xp5k/rake'


# Constants
#
SSH_CONFIGFILE_OPT = XP5K::Config[:ssh_config].nil? ? "" : " -F " + XP5K::Config[:ssh_config]


# XP5K configuration (xp.conf)
#
XP5K::Config['site']     ||= 'rennes'
XP5K::Config['walltime'] ||= '1:00:00'
XP5K::Config['jobname']  ||= 'xp5k-kameleon-builder'


# Initialize experiment
#
@xp = XP5K::XP.new()
def xp; @xp; end


# XP5K job
#
resources = [] << %{/nodes=1,walltime=#{XP5K::Config[:walltime]}}
roles = [
  XP5K::Role.new({
    name: 'builder',
    size: 1
  })
]

@job_def = {
  resources:  resources,
  site:       XP5K::Config[:site],
  types:      ['deploy'],
  name:       XP5K::Config[:jobname],
  roles:      roles,
  command:    'sleep 186400'
}

@job_def[:reservation] = XP5K::Config[:reservation] unless XP5K::Config[:reservation].nil?
xp.define_job(@job_def)


# XP5K deployment
#
@deployment_def = {
  site:        XP5K::Config[:site],
  environment: 'jessie-x64-base',
  jobs:        [XP5K::Config[:jobname]],
  key:         File.read(XP5K::Config[:public_key])
}

@deployment_def[:notifications] = ["xmpp:#{XP5K::Config[:user]}@jabber.grid5000.fr"] if XP5K::Config[:notification]
xp.define_deployment(@deployment_def)


# Job management tasks
#
namespace :grid5000 do

  desc 'Submit OAR jobs'
  task :jobs do
    xp.submit
    xp.wait_for_jobs
  end

  desc 'Get OAR jobs status'
  task :status do
    xp.status
  end

  desc 'Clean all OAR jobs'
  task :clean do
    puts "Clean all Grid'5000 running jobs..."
    xp.clean
  end

end


# Deployment management tasks
#
namespace :grid5000 do

  desc 'Submit Kadeploy environment deployment'
  task :deploy do
    xp.deploy
  end

end


# Shell tasks
#

# Fork the execution of a command. Used to execute ssh on deployed nodes.
def fork_exec(command, *args)
  # Remove empty args
  args.select! { |arg| arg != "" }
  args.flatten!
  pid = fork do
    Kernel.exec(command, *args)
  end
  Process.wait(pid)
end

# Parse host environment var
def parse_host
  args = ENV['host'].split(',')
  hosts = []
  args.each do |arg|
    if XP5K::Role.listnames.include? arg
      hosts << roles(arg)
    else
      hosts << arg
    end
  end
  hosts.flatten
end


desc "ssh on host, need host=<role|FQDN>"
task :shell do
  host = parse_host().first
  fork_exec('ssh', SSH_CONFIGFILE_OPT.split(" "), 'root@' + host)
end

desc 'Launch command in parallel, need cmd=<command> and host=<role|FQDN>'
task :cmd do
  abort "Need cmd=" unless cmd = ENV['cmd']
  user = ENV['user'] || 'root'
  hosts = parse_host()
  on hosts, :user => user do
    cmd
  end
end

namespace :kameleon do

  task :install do
    on(roles('builder'), :user => 'root', :environment => { DEBIAN_FRONTEND: 'noninteractive' }) do
      cmds = [] << 'echo "deb http://moais.imag.fr/membres/vincent.danjean/debian stable main imag" >> /etc/apt/sources.list'
      cmds << 'apt-get update'
      cmds << 'apt-get install -y ruby-dev ruby-childprocess polipo git-core'
      cmds << 'apt-get install -y --allow-unauthenticated libguestfs-tools'
#      cmds << 'apt-get install -t jessie-backports -y libguestfs-tools'
      cmds << 'apt-get install -y virtualbox linux-headers-amd64 socat'
      cmds << 'gem install --no-ri --no-rdoc kameleon-builder'
      cmds << 'mount -t tmpfs tmpfs /tmp'
#      cmds << 'mv /bin/gzip /bin/gzip.OLD'
#      cmds << 'ln -s /usr/bin/pigz /bin/gzip'
      cmds
    end
  end

  task :repos do
    on(roles('builder'), :user => 'root') do
      cmds = [] << 'kameleon template repo add default https://github.com/oar-team/kameleon-recipes.git'
      cmds << 'kameleon template repo add grid5000 https://github.com/grid5000/environments-recipes.git'
      cmds << 'git clone https://github.com/oar-team/kameleon-recipes.git'
      cmds << 'cd kameleon-recipes && git checkout -b refactor origin/refactor && kameleon template repo add refactor file:///root/kameleon-recipes/ && cd -'
      cmds
    end
  end

  task :deploy_recipes do
    host = roles('builder').first
    sh %{tar -cf - recipes/ | ssh#{SSH_CONFIGFILE_OPT} root@#{host} 'cd /tmp && tar xf -'}
    sh %{tar -cf - g5k-recipes/ | ssh#{SSH_CONFIGFILE_OPT} root@#{host} 'cd /tmp && tar xf -'}
  end

  task :build do
    abort "Need name=" unless name = ENV['name']
    on(roles('builder'), :user => 'root') do
      %{cd recipes && kameleon build #{name}.yaml}
    end
  end


end

task :run do
  workflow = [
    'grid5000:jobs',
    'grid5000:deploy',
    'kameleon:install',
    'kameleon:repos',
    'kameleon:deploy_recipes'
  ]
  workflow.each do |task|
    Rake::Task[task].execute
  end
end

